# Payroll Project

Built using the CodeIgniter PHP Framework

**Dependencies:**
*  MySQL 5.5
*  PHP 5.6

**Instructions**
1.  Clone the repository
2.  Duplicate `index.php.copy.php` to `index.php`
3.  Duplicate `.htaccess.copy` to `.htaccess`
4.  Duplicate `application/config/database.php.copy.php` to `database.php`